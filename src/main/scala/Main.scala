import java.io.File
import org.docx4j.jaxb.Context
import org.docx4j.openpackaging.packages.WordprocessingMLPackage

object Main extends App {

  val wordMLPackage = WordprocessingMLPackage.createPackage()
  wordMLPackage.getMainDocumentPart.addParagraphOfText("Artsiom Miklushou")


  wordMLPackage.save(new File("helloworld.docx"))

}
